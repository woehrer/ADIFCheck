from pyArango.connection import Connection
from tkinter import messagebox
import sys
import logging
import time
import datetime
import helper.hamradio
import helper.functions
import helper.hamqth_callsign
from pyhamtools import locator

class DATABASE():

    def __init__(self):
        pass

    def connect(self, url ,user, password):
        try:
            self.conn = Connection(arangoURL=url, username=user, password=password)
        except ZeroDivisionError:
            print("ZeroficisionError")
        except TypeError:
            errortext = "TypeError: Connection to Database failed\nURL: " + url + "\nUser: " + user + "\nPassword: " + password + "\n" + str(sys.exc_info())
            print(errortext)
            logging.error(errortext)
            messagebox.showerror(message=errortext, title = "Error")

    def setup(self):
            if not self.conn.hasDatabase(name="ADIFCheck"):
                db = self.conn.createDatabase(name="ADIFCheck")
            db = self.conn['ADIFCheck']

            if not db.hasCollection(name="wsjtx"):
                db.createCollection(name="wsjtx")
            wsjtxCollection = db["wsjtx"]
            wsjtxCollection.truncate()

            if not db.hasCollection(name="Callsigns"):
                db.createCollection(name="Callsigns")

            if not db.hasCollection(name="adif"):
                db.createCollection(name="adif")
                db['adif'].ensureFulltextIndex(['CALL'], name= "CALL")
                db['adif'].ensureFulltextIndex(['ITUZ'], name= "ITUZ")
                db['adif'].ensureFulltextIndex(['CQZ'], name= "CQZ")
                db['adif'].ensureFulltextIndex(['DXCC'], name= "DXCC")

            if not db.hasCollection(name="MostWanted"):
                db.createCollection(name="MostWanted")

            if not db.hasCollection(name="searchList"):
                db.createCollection(name="searchList")

            if not db.hasCollection(name="Telnet"):
                db.createCollection(name="Telnet")
            TelnetCollection = db['Telnet']
            TelnetCollection.truncate()

            if not db.hasCollection(name="HamqthCluster"):
                db.createCollection(name="HamqthCluster")
            HamqthClusterCollection = db['HamqthCluster']
            HamqthClusterCollection.truncate()

    def checkEmptyKeys(self,data):
        if 'GRIDSQUARE' in data and not 'LAT' in data and not 'LON' in data:
            if helper.functions.IsAGridsquare(data['GRIDSQUARE']):
                data['LAT'],data['LON'] = locator.locator_to_latlong(data['GRIDSQUARE'])
        databasedata = self.getallKeys(data['CALL_SENT'])
        if databasedata is not False:
            databasedata = databasedata[0]
            if 'ITUZ' in databasedata and 'ITUZ' not in data:
                data['ITUZ'] = databasedata['ITUZ']
            if 'DXCC' in databasedata and 'DXCC' not in data:
                data['DXCC'] = databasedata['DXCC']
            if 'CQZ' in databasedata and 'CQZ' not in data:
                data['CQZ'] = databasedata['CQZ']
            if 'GRIDSQUARE' in databasedata and 'GRIDSQUARE' not in data:
                data['GRIDSQUARE'] = databasedata['GRIDSQUARE']
            if 'LAT' in databasedata and 'LAT' not in data:
                data['LAT'] = databasedata['LAT']
            if 'LON' in databasedata and 'LON' not in data:
                data['LON'] = databasedata['LON']
            if 'CONT' in databasedata and 'CONT' not in data:
                data['CONT'] = databasedata['CONT']
            if 'COUNTRY' in databasedata and 'COUNTRY' not in data:
                data['COUNTRY'] = databasedata['COUNTRY']
            if 'LOTW_USER' in databasedata and 'LOTW_USER' not in data:
                data['LOTW_USER'] = databasedata['LOTW_USER']
            if 'EQSL_USER' in databasedata and 'EQSL_USER' not in data:
                data['EQSL_USER'] = databasedata['EQSL_USER']
        return data

    def insertWsjtxQso(self,data):
        db = self.conn['ADIFCheck']
        wsjtxCollection = db['wsjtx']
        wsjtxQso = wsjtxCollection.createDocument()
        data['CALL_SENT'] = data['CALL_SENT'].strip('<').strip('>')
        CallSentDXCC = helper.hamradio.searchDXCC(data['CALL_SENT'])
        if CallSentDXCC is False:
            logging.warning("Kein DXCC gefunden: " + data['CALL_SENT'])
        else:
            wsjtxQso['DXCC_SENT'] = CallSentDXCC
        if 'CALL_RCVD' in data:
            data['CALL_RCVD'] = data['CALL_RCVD'].strip('<').strip('>')
            CallRCVDDXCC = helper.hamradio.searchDXCC(data['CALL_RCVD'])
            if CallRCVDDXCC is False:
                logging.warning("Kein DXCC gefunden: " + data['CALL_RCVD'])
            else:
                wsjtxQso['DXCC_RCVD'] = CallRCVDDXCC
            
        wsjtxQso['TIMESTAMP'] = datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S")
        
        wsjtxQso['CALL'] = data['CALL_SENT']

        for keys in data:
            wsjtxQso[keys] = data[keys]

        self.insertCallsign(data['CALL_SENT'],wsjtxQso)

        wsjtxQso = self.checkEmptyKeys(wsjtxQso)
        wsjtxQso.save()
    
    def insertADIF(self,data):
        db = self.conn['ADIFCheck']
        adifCollection = db['adif']
        adifQso = adifCollection.createDocument()
        for keys in data:
            adifQso[keys] = data[keys]
        self.insertCallsign(data['CALL'],adifQso)
        adifQso = self.checkEmptyKeys(adifQso)
        adifQso.save()

    def insertCallsign(self,Call,data):
        db = self.conn['ADIFCheck']
        CallsignCollection = db['Callsigns']
        check = self.searchCallCallsigns(Call)
        if not check:
        # Create new Document
            CallsignDoc = CallsignCollection.createDocument()
            CallsignDoc['CALL'] = Call
        else:
            CallsignDoc = CallsignCollection[check]
        if 'ITUZ' in data:
            CallsignDoc['ITUZ'] = data['ITUZ']
        if 'DXCC' in data:
            CallsignDoc['DXCC'] = data['DXCC']
        if 'CQZ' in data:
            CallsignDoc['CQZ'] = data['CQZ']
        if 'GRIDSQUARE' in data:
            CallsignDoc['GRIDSQUARE'] = data['GRIDSQUARE']
        if 'LAT' in data:
            CallsignDoc['LAT'] = data['LAT']
        if 'LON' in data:
            CallsignDoc['LON'] = data['LON']
        if 'CONT' in data:
            CallsignDoc['CONT'] = data['CONT']
        if 'COUNTRY' in data:
            CallsignDoc['COUNTRY'] = data['COUNTRY']
        if 'LOTW_USER' in data:
            CallsignDoc['LOTW_USER'] = data['LOTW_USER']
        if 'EQSL_USER' in data:
            CallsignDoc['EQSL_USER'] = data['EQSL_USER']
        if 'checked' in data:
            CallsignDoc['checked'] = data['checked']
        CallsignDoc = self.checkEmptyKeys(CallsignDoc)
        CallsignDoc.save()

    def insertMostWanted(self,data):
        db = self.conn['ADIFCheck']
        MostWantedCollection = db['MostWanted']
        MostWantedCollection.truncate()
        MostWantedDocument = MostWantedCollection.createDocument()
        for keys in data:
            MostWantedDocument[keys] = data[keys]
        MostWantedDocument.save()

    def insertSearchlist(self,data):
        db = self.conn['ADIFCheck']
        searchListCollection = db['searchList']
        searchListCollection.truncate()
        searchListDocument = searchListCollection.createDocument()
        for keys in data:
            searchListDocument[keys] = data[keys]
        searchListDocument.save()

    def insertTelnet(self,data):
        db = self.conn['ADIFCheck']
        TelnetCollection = db['Telnet']
        TelnetDocument = TelnetCollection.createDocument()
        TelnetDocument['TIMESTAMP'] = datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S")
        for keys in data:
            TelnetDocument[keys] = data[keys]
        self.insertCallsign(data['CALL_SENT'],TelnetDocument)
        TelnetDocument = self.checkEmptyKeys(TelnetDocument)
        TelnetDocument.save()

    def insertHamqthCluster(self,data):
        db = self.conn['ADIFCheck']
        HamqthClusterCollection = db['HamqthCluster']
        HamqthClusterDocument = HamqthClusterCollection.createDocument()
        HamqthClusterDocument['TIMESTAMP'] = time.gmtime(time.time())
        for keys in data:
            HamqthClusterDocument[keys] = data[keys]
        self.insertCallsign(data['CALL_SENT'],HamqthClusterDocument)
        HamqthClusterDocument = self.checkEmptyKeys(HamqthClusterDocument)
        HamqthClusterDocument.save()
        
    def checkCallsigns(self):
        db = self.conn['ADIFCheck']
        checkCallsignsCollection = db['Callsigns']
        aql = "for x in Callsigns Filter x.checked != '1' Return x.CALL"
        queryResult = db.AQLQuery(aql, rawResults=True)
        for call in queryResult:
            data = helper.hamqth_callsign.callsign(call)
            if data is  not False:
                data['checked'] = "1"
                self.insertCallsign(call,data)

    def deleteADIF(self):
        db = self.conn['ADIFCheck']
        adifCollection = db['adif']
        adifCollection.truncate()
        logging.info("ADIF deleted")

    def getSearchList(self,):
        db = self.conn['ADIFCheck']
        aql = "For x in searchList Return x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        if len(queryResult) == 0:
            return False
        return queryResult

    def getcountADIF(self):
        db = self.conn['ADIFCheck']
        adifCollection = db['adif']
        return adifCollection.count()

    def getcountDXCC(self,Dxcc):
        db = self.conn['ADIFCheck']
        aql = 'For x in adif Filter x.DXCC == "' + str(Dxcc) + '" COLLECT WITH COUNT INTO length RETURN length'
        queryResult = db.AQLQuery(aql, rawResults=True)
        return queryResult[0]

    def getlast10Telnet(self,):
        db = self.conn['ADIFCheck']
        aql = "For x in Telnet Sort x.TIMESTAMP DESC LIMIT 10 Return x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        if len(queryResult) == 0:
            return False
        return queryResult

    def getlast10Cluster(self,):
        db = self.conn['ADIFCheck']
        aql = "For x in HamqthCluster Sort x.TIMESTAMP DESC LIMIT 10 Return x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        if len(queryResult) == 0:
            return False
        return queryResult

    def getlast10wsjtx(self,):
        db = self.conn['ADIFCheck']
        aql = "For x in wsjtx Sort x.TIMESTAMP DESC LIMIT 10 Return x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        if len(queryResult) == 0:
            return False
        return queryResult

    def getallKeys(self,CALL):
        db = self.conn['ADIFCheck']
        aql = "FOR x IN Callsigns FILTER x.CALL == '" + str(CALL) + "' RETURN x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        if len(queryResult) == 0:
            return False
        return queryResult

    def searchCallCallsigns(self,CALL):
        db = self.conn['ADIFCheck']
        aql = "FOR x IN Callsigns FILTER x.CALL == '" + str(CALL) + "' RETURN x._key"
        queryResult = db.AQLQuery(aql, rawResults=True)
        if len(queryResult) == 0:
            return False
        return queryResult[0]

    def searchCallAdif(self,CALL,BAND,BAND_CHECK):
        db = self.conn['ADIFCheck']
        if BAND_CHECK is False:
            aql = "FOR x IN adif FILTER x.CALL == '" + str(CALL) + "' RETURN x"
        else:
            aql = "FOR x IN adif FILTER x.CALL == '" + str(CALL) + "' and x.BAND == '" + str(BAND) + "' RETURN x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        if len(queryResult) == 0:
            return False
        return True

    def searchCQZ(self,CQZ,BAND,BAND_CHECK):
        db = self.conn['ADIFCheck']
        if BAND_CHECK is False:
            aql = "FOR x IN adif FILTER x.CQZ == '" + str(CQZ) + "' RETURN x"
        else:
            aql = "FOR x IN adif FILTER x.CQZ == '" + str(CQZ) + "' and x.BAND == '" + str(BAND) + "' RETURN x"
        queryResult = db.AQLQuery(aql, rawResults=True, batchSize=1000000)
        if len(queryResult) == 0:
            return False
        return True

    def searchITU(self,ITUZ,BAND,BAND_CHECK):
        db = self.conn['ADIFCheck']
        if BAND_CHECK is False:
            aql = "FOR x IN adif FILTER x.ITUZ == '" + str(ITUZ) + "' RETURN x"
        else:
            aql = "FOR x IN adif FILTER x.ITUZ == '" + str(ITUZ) + "' and x.BAND == '" + str(BAND) + "' RETURN x"
        queryResult = db.AQLQuery(aql, rawResults=True, batchSize=1000000)
        if len(queryResult) == 0:
            return False
        return True

    def searchDXCC(self,DXCC,BAND,BAND_CHECK):
        db = self.conn['ADIFCheck']
        if BAND_CHECK is False:
            aql = "FOR x IN adif FILTER x.DXCC == '" + str(DXCC) + "' RETURN x"
        else:
            aql = "FOR x IN adif FILTER x.DXCC == '" + str(DXCC) + "' and x.BAND == '" + str(BAND) + "' RETURN x"
        queryResult = db.AQLQuery(aql, rawResults=True)
        if len(queryResult) == 0:
            return False
        return True
