import telnetlib
import re
import logging
from tkinter import messagebox
import sys
#import helper.hamradio
#import helper.adif

class TELNET():
    def __init__(self):
        callsign_pattern = "([a-z|0-9|/]+)"
        frequency_pattern = "([0-9|.]+)"
        self.pattern = re.compile("^DX de "+callsign_pattern+":\s+"+frequency_pattern+"\s+"+callsign_pattern+"\s+(.*)\s+(\d{4}Z)", re.IGNORECASE)

    def connect(self,Server,Port,OwnCall):
        try:
            self.tn = telnetlib.Telnet(Server)
            self.tn.read_until(b":")
            self.tn.write((OwnCall + "\n").encode('ascii'))
        except ConnectionResetError:
            errortext = "ConnectionResetError: Connection to Telnet Reset\nServer: " + Server + "\nCall: " + OwnCall + "\nNo Call in Config? \n" + str(sys.exc_info())
            print(errortext)
            logging.error(errortext)
            messagebox.showerror(message=errortext, title = "Error")

    def decode(self):
        try:
            data = {}
            telnet_output = self.tn.read_until(b"\n")
            output = telnet_output.decode('ascii')
            if output.find('DX de') == 0:
                #print(output[6:])
                output_split = output[6:].split()
                if "FT8" in output_split:
                    data['CALL_RCVD'] = output_split[0].replace(":","")
                    data['FREQ'] = output_split[1]
                    data['CALL_SENT'] = output_split[2]

                    return data
            return False
        except ConnectionResetError:
            errortext = "ConnectionResetError: Connection to Telnet Reset\nNo Call in Config? \n" + str(sys.exc_info())
            print(errortext)
            logging.error(errortext)
            messagebox.showerror(message=errortext, title = "Error")
            return False