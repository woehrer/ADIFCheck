import logging
import tkinter
import requests
import CLASS.DATABASE
import sys
import helper.hamradio

def IsACall(Call):
    if isinstance(Call,str):
        # 1
        if len(Call) <= 3:
            logging.warning("isACALL: # 1 "+ Call)
            return False
        # 2
        if Call[0].isdecimal():
            if Call[1].isdecimal():
                logging.warning("isACALL: # 2 "+ Call)
                return False
        # 3
        if Call[-1].isnumeric():
            logging.warning("isACALL: # 3 "+ Call)
            return False
        # 4
        if Call[0].isalpha():
            if Call[1].isalpha():
                if Call[2].isdecimal():
                    if Call[3].isdecimal():
                        logging.warning("isACALL: # 4 "+ Call)
                        return False
        # 5
        if Call[3].isdecimal():
            if Call[2].isdecimal():
                logging.warning("isACALL: # 5 "+ Call)
                return False
        return True
    logging.error("isaCall Not a String")
    return False

def IsAGridsquare(Grid):
    if isinstance(Grid,str):
        if len(Grid) == 4:
            if Grid[0].isalpha():
                if Grid[1].isalpha():
                    if Grid[2].isnumeric():
                        if Grid[3].isnumeric():
                            return True
        if len(Grid) == 6:
            if Grid[0].isalpha():
                if Grid[1].isalpha():
                    if Grid[2].isnumeric():
                        if Grid[3].isnumeric():
                            if Grid[4].isalpha():
                                if Grid[5].isalpha():
                                    return True
    try:                                
        logging.warning("Not found Grid" + Grid)
    except TypeError:
        logging.error("TypeError: Its not a String in isAGridsquare" + str(sys.exc_info()))
        print("TypeError: Its not a String in isAGridsquare" + str(sys.exc_info()))
    return False

def convert_freq_to_band(freq):
    if 0.1357 <= freq <= 0.1378:
        return '2190m'
    elif 0.472 <= freq <= 0.479:
        return '630m'
    elif 0.501 <= freq <= 0.504:
        return '560m'
    elif 1.8 <= freq <= 2.0:
        return '160m'
    elif 3.5 <= freq <= 4.0:
        return '80m'
    elif 5.06 <= freq <= 5.45:
        return '60m'
    elif 7.0 <= freq <= 7.3:
        return '40m'
    elif 10.1 <= freq <= 10.15:
        return '30m'
    elif 14.0 <= freq <= 14.35:
        return '20m'
    elif 18.068 <= freq <= 18.168:
        return '17m'
    elif 21.0 <= freq <= 21.45:
        return '15m'
    elif 24.89 <= freq <= 24.99:
        return '12m'
    elif 28.0 <= freq <= 29.7:
        return '10m'
    elif 50 <= freq <= 54:
        return '6m'
    elif 70 <= freq <= 71:
        return '4m'
    elif 144 <= freq <= 148:
        return '2m'
    elif 222 <= freq <= 225:
        return '1.25m'
    elif 420 <= freq <= 450:
        return '70cm'
    elif 902 <= freq <= 928:
        return '33cm'
    elif 1240 <= freq <= 1300:
        return '23cm'
    elif 2300 <= freq <= 2450:
        return '13cm'
    elif 3300 <= freq <= 3500:
        return '9cm'
    elif 5650 <= freq <= 5925:
        return '6cm'
    elif 10000 <= freq <= 10500:
        return '3cm'
    elif 24000 <= freq <= 24250:
        return '1.25cm'
    elif 47000 <= freq <= 47200:
        return '6mm'
    elif 75500 <= freq <= 81000:
        return '4mm'
    elif 119980 <= freq <= 120020:
        return '2.5mm'
    elif 142000 <= freq <= 149000:
        return '2mm'
    elif 241000 <= freq <= 250000:
        return '1mm'
    return None

def initlogger():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    handler = logging.FileHandler("logs/ADIFCheck.log")
    formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
    handler.setFormatter(formatter)
    handler.setLevel(logging.INFO)
    logger.addHandler(handler)

def MostWantedList(databaseurl ,user, password):
    Database = CLASS.DATABASE.DATABASE()

    Database.connect(databaseurl ,user, password)

    url = "https://secure.clublog.org/mostwanted.php?api=1"

    httpx = requests.get(url)

    MostWantedJson = httpx.json()

    Database.insertMostWanted(MostWantedJson)


def searchForColor(databaseurl ,user, password,Call,Band):
    Database = CLASS.DATABASE.DATABASE()

    Database.connect(databaseurl ,user, password)

    CallDXCC = helper.hamradio.searchDXCC(Call)
    #Call on Band
    if CallDXCC is False:
        backgroundColor = "red"
        textColor = "white"
    elif Database.searchCallAdif(Call,Band,False):
        backgroundColor = "green"
        textColor = "black"
    #New Call on Band
    elif Database.searchCallAdif(Call,Band,True):
        backgroundColor = "#ffff60"
        textColor = "black"
    #New DXCC on Band
    elif Database.searchDXCC(CallDXCC['entityCode'],Band,True):
        backgroundColor = "#ff8060"
        textColor = "black"
    #New ITU Zone on Band
    elif Database.searchITU(CallDXCC['itu'],Band,True):
        backgroundColor = "#ff4060"
        textColor = "black"
    #New CQ Zone on Band
    elif Database.searchCQZ(CallDXCC['cq'],Band,False):
        backgroundColor = "#ff0060"
        textColor = "black"
    #New Call
    elif Database.searchCallAdif(Call,Band,False):
        backgroundColor = "#ffff00"
        textColor = "black"
    #New DXCC
    elif Database.searchDXCC(CallDXCC['entityCode'],Band,False):
        backgroundColor = "#ff8000"
        textColor = "black"
    #New ITU
    elif Database.searchITU(CallDXCC['itu'],Band,False):
        backgroundColor = "#ff4000"
        textColor = "black"
    #New CQ Zone
    elif Database.searchCQZ(CallDXCC['cq'],Band,False):
        backgroundColor = "red"
        textColor = "black"
    #Nothing Found
    else:
        backgroundColor = "red"
        textColor = "white"

    return backgroundColor, textColor