from fake_headers import Headers

import requests
import sys
import datetime
import logging

from tkinter import messagebox

import helper.config

import CLASS.DATABASE

#Header fälschen
url = "https://www.hamqth.com/dxc_csv.php"

#Call^Frequency^Date/Time^Spotter^Comment^LoTW user^eQSL user^Continent^Band^Country name

def csv():
    conf = helper.config.initconfig()

    Database = CLASS.DATABASE.DATABASE()
    Database.connect(conf['DATABASE_URL'],'root',conf['DATABASE_PW'])

    headers = Headers(os="mac", headers=True).generate()
    try:
        httpx = requests.get(url, headers=headers)

        antwort = httpx.text

        for line in antwort.split('\n'):
            if len(line) > 3:
                data = {}
                part = line.split("^")
                data['CALL_SENT'] = part[0]
                data['FREQ'] = part[1]
                data['CALL_RCVD'] = part[2]
                data['COMMENT'] = part[3]
                data['TIMESTAMP'] = datetime.datetime.strptime(part[4],'%H%M %Y-%m-%d')
                data['LOTW_USER'] = part[5]
                data['EQSL_USER'] = part[6]
                data['CONT'] = part[7]
                data['BAND'] = part[8].replace("M","m")
                data['COUNTRY'] = part[9]
                data['DXCC'] = part[10]

                if data['COMMENT'].find("FT8") >= 0:
                    Database.insertHamqthCluster(data)
    except ConnectionError:
            errortext = "ConnectionError: Connection to HamQTH Cluster failed\n" + str(sys.exc_info())
            print(errortext)
            logging.error(errortext)
            messagebox.showerror(message=errortext, title = "Error")