from tkinter import BooleanVar
from hamutils.adif import ADIReader
import datetime
from collections import Counter
import CLASS.DATABASE

QsoJson = {
    'ADIF_VER' : 312,
    'ADDRESS' : "",
    'ADDRESS_INTL' : "",
    'AGE' : 0,
    'A_INDEX' : 0,
    'ANT_AZ' : 0,
    'ANT_EL' : 0,
    'ANT_PATH' : 0,
    'ARRL_SECT' : "",
    'AWARD_SUBMITTED' : "",
    'AWARD_GRANTED' : "",
    'BAND' : "",
    'BAND_RX' : "",
    'CALL' : "",
    'CHECK' : "",
    'CLASS' : "",
    'CLUBLOG_QSO_UPLOAD_DATE' : datetime.datetime,
    'CLUBLOG_QSO_UPLOAD_STATUS' : "",
    'CNTY' : "",
    'COMMENT' : "",
    'COMMENT_INTL' : "",
    'CONT' : "",
    'CONTACTED_OP' : "",
    'CONTEST_ID' : "",
    'COUNTRY' : "",
    'COUNTRY_INTL' : "",
    'CQZ' : 0,
    'CREDIT_SUBMITTED' : [],
    'CREDIT_GRANTED' : [],
    'DARC_DOK' : "",
    'DISTANCE' : 0,
    'DXCC' : "",
    'EMAIL' : "",
    'EQ_CALL' : "",
    'EQSL_QSLRDATE' : datetime.datetime,
    'EQSL_QSLSDATE' : datetime.datetime,
    'EQSL_QSL_RCVD' : "",
    'EQSL_QSL_SENT' : "",
    'FISTS' : 0,
    'FISTS_CC' : 0,
    'FORCE_INIT' : 0,
    'FREQ' : 0,
    'FREQ_RX' : 0,
    'GRIDSQUARE' : "",
    'GUEST_OP' : "",
    'HRDLOG_QSO_UPLOAD_DATE' : datetime.datetime,
    'HRDLOG_QSO_UPLOAD_STATUS' : "",
    'IOTA' : "",
    'IOTA_ISLAND_ID' : 0,
    'ITUZ' : 0.0,
    'K_INDEX' : 0,
    'LAT' : "",
    'LON' : "",
    'LOTW_QSLRDATE' : datetime.datetime,
    'LOTW_QSLSDATE' : datetime.datetime,
    'LOTW_QSL_RCVD' : "",
    'LOTW_WSL_SEND' : "",
    'MAX_BURSTS' : 0,
    'MODE' : "",
    'MS_SHOWER' : "",
    'MY_ANTENNA' : "",
    'MY_ANTENNA_INTL' : "",
    'MY_CITY' : "",
    'MY_CITY_INTL' : "", 
    'MY_COUNTRY' : "",
    'MY_COUNTRY_INTL' : "",
    'MY_CQ_ZONE' : "",
    'MY_DXCC' : "",
    'MY_FISTS' : "",
    'MY_GRIDSQUARE' : "",
    'MY_IOTA' : "",
    'MY_IOTA_ISLAND_ID' : 0,
    'MY_ITU_ZONE' : 0,
    'MY_LAT' : "",
    'MY_LON' : "",
    'MY_NAME' : "",
    'MY_NAME_INTL' : "",
    'MY_POSTAL_CODE' : "",
    'MY_POSTAL_CODE_INTL' : "",
    'MY_RIG' : "",
    'MY_RIG_INTL' : "",
    'MY_SIG' : "",
    'MY_SIG_INTL' : "",
    'MY_SIG_INFO' : "",
    'MY_SIG_INFO_INTL' : "",
    'MY_SOTA_REG' : "",
    'MY_STATE' : "",
    'MY_STREET' : "",
    'MY_STREET_INTL' : "",
    'MY_USACA_CONTIES' : [],
    'MY_VUCC_GRIDS' : [],
    'NAME' : "",
    'NAME_INTL' : "",
    'NOTES' : "",
    'NOTES_INTL' : "",
    'NR_BURSTS' : 0,
    'NR_PINGS' : 0,
    'OPERATOR' : 0,
    'OWNER_CALLSIGN' : "",
    'PFX' : "",
    'PRECEDENCE' : "",
    'PROP_MODE' : "",
    'PUBLIC_KEY' : "",
    'QRZCOM_QSO_UPLOAD_DATE' : datetime.datetime,
    'QRZCOM_QSO_UPLOAD_STATUS' : "",
    'QSLMSG' : "",
    'QSLMSG_INTL' : "",
    'QSLRDATE' : datetime.datetime,
    'QSLSDATE' : datetime.datetime,
    'QSL_RCVD' : "",
    'QSL_RCVD_VIA' : "",
    'QSL_SENT' : "",
    'QSL_SENT_VIA' : "",
    'QSL_VIA' : "",
    'QSO_COMPLETE' : "",
    'QSO_DATE' : datetime.datetime,
    'QSO_DATE_OFF' : datetime.datetime,
    'QSO_RANDOM' : BooleanVar,
    'QTH' : "",
    'QTH_INTL' : "",
    'REGION' : "",
    'RIG' : "",
    'RIG_INTL' : "",
    'RST_RCVD' : "",
    'RST_SENT' : "",
    'RX_PWR' : 0,
    'SAT_MODE' : "",
    'SAT_NAME' : "",
    'SFI' : 0,
    'SIG' : "",
    'SIG_INTL' : "",
    'SILENT_KEY' : BooleanVar,
    'SKCC' : "",
    'SOTA_REF' : "",
    'SRX' : 0,
    'SRX_STRING' : "",
    'STATE' : "",
    'STATION_CALLSIGN' : "",
    'STX' : 0,
    'STX_STRING' : "",
    'SUBMODE' : "",
    'SWL' : BooleanVar,
    'TEN_TEN' : 0,
    'TIME_OFF' : datetime.datetime,
    'TIME_ON' : datetime.datetime,
    'TX_PWR' : 0,
    'UKSMG' : 0,
    'USACA_COUNTIED' : [],
    'VE_PROV' : "",
    'VUCC_GRIDS' : [],
    'WEB' : "",
}

def read(path,databaseurl ,user, password):
    Database = CLASS.DATABASE.DATABASE()
    Database.connect(databaseurl ,user, password)
    f=open(path, 'r', encoding="ascii")
    adi = ADIReader(f)
    Database.deleteADIF()
    for qsoadi in adi:
        Qso_Json = QSOtoJSON(qsoadi)
        Database.insertADIF(Qso_Json)
    return Database.getcountADIF()

def QSOtoJSON(qso):
    QsoJ = {}
    QsoJ['CALL'] = qso['call']
    if 'gridsquare' in qso:
        QsoJ['GRIDSQUARE'] = qso['gridsquare']
    QsoJ['MODE'] = qso['mode']
    QsoJ['RST_SENT'] = qso['rst_sent']
    QsoJ['RST_RCVD'] = qso['rst_rcvd']
    band = qso['band'].replace("M","m")
    band = band.replace("Cm","cm")
    QsoJ['BAND'] = band
    QsoJ['FREQ'] = qso['freq']
    if 'station_callsign' in qso:
        QsoJ['STATION_CALLSIGN'] = qso['station_callsign']
    QsoJ['MY_GRIDSQUARE'] = qso['my_gridsquare']
    if 'comment' in qso:
        QsoJ['COMMENT'] = qso['comment']
    QsoJ['QSO_DATE'] = qso['datetime_on']
    QsoJ['QSO_DATE_OFF'] = qso['datetime_off']
    if 'qso_sent' in qso is not None:
        QsoJ['QSO_SENT'] = qso['qso_sent']
    if 'qso_rcvd' in qso:
        QsoJ['QSO_RCVD'] = qso['qso_rcvd']
    if 'my_gridsquare' in qso:
        QsoJ['MY_GRIDSQUARE'] = qso['my_gridsquare']
    if 'app_cqrlog_dxcc' in qso:
        QsoJ['REGION'] = qso['app_cqrlog_dxcc']
    if 'dxcc' in qso:
        QsoJ['DXCC'] = qso['dxcc']
    if 'ituz' in qso:
        QsoJ['ITUZ'] = qso['ituz']
    if 'cqz' in qso:
        QsoJ['CQZ'] = qso['cqz']
    if 'cont' in qso:
        QsoJ['CONT'] = qso['cont']

    return {**QsoJ}

# Liste = read('/home/woehrer/Dokumente/cqrlog.ADI')
# print(getCountQSO(Liste))
# print(getCount(Liste,'MODE'))
# print(getCount(Liste,'DXCC'))
