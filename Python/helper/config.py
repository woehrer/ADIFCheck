import configparser
import logging
import sys
import os
from tkinter import messagebox
import helper.functions

config = configparser.ConfigParser()
configFile = "config/config.ini"

def initconfig():
    if os.path.isfile(configFile):
        pass
    else:
        create()
    #Konfigdatei initialisieren
    try:
        #Config Datei auslesen
        config.read(configFile)
        conf = config['DEFAULT']
        return conf
    except:
        messagebox.showerror(message="Error while loading the Config file" , title = "Error")
        print("Error while loading the Config file:" + str(sys.exc_info()))
        logging.error("Error while loading the Config file" + str(sys.exc_info()))

def create():
    
    config['DEFAULT'] = {'CALLSIGN' : '',
                        'lastPath' : '',
                        'MULTICAST_IP' : '224.0.0.0',
                        'MULTICAST_PORT' : 2237,
                        'MULTICAST_ENABLE': True,
                        'DATABASE_PW' : "openSesame",
                        'DATABASE_URL' : "http://127.0.0.1:8529",
                        'NEW_CQ_ZONE_ALERT_ENABLE' : False,
                        'NEW_ITU_ZONE_ALERT_ENABLE' : False,
                        'NEW_DXCC_ALERT_ENABLE' : False,
                        'NEW_CALL_ALERT_ENABLE' : False,
                        'NEW_CQ_ZONE_ON_BAND_ALERT_ENABLE' : False,
                        'NEW_ITU_ZONE_ON_BAND_ALERT_ENABLE' : False,
                        'NEW_DXCC_ON_BAND_ALERT_ENABLE' : False,
                        'NEW_CALL_ON_BAND_ALERT_ENABLE' : False,
                        'TELNET_SERVER': 'telnet.reversebeacon.net',
                        'TELNET_PORT' : 23}

    with open(configFile, 'w') as configfile:
        config.write(configfile)

def setCallsign(text):
    config.set('DEFAULT','CALLSIGN',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setlastpath(text):
    config.set('DEFAULT','lastPath',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setMulticastIP(text):
    config.set('DEFAULT','MULTICAST_IP',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setMulticastPort(number):
    config.set('DEFAULT','MULTICAST_PORT',number)
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setMulticastEnable(boolvalue):
    config.set('DEFAULT','MULTICAST_ENABLE',str(boolvalue))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setdatabasePW(text):
    config.set('DEFAULT','DATABASE_PW',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setdatabaseURL(text):
    config.set('DEFAULT','DATABASE_URL',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setNewCQZoneAlertEnable(text):
    config.set('DEFAULT','NEW_CQ_ZONE_ALERT_ENABLE',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setNewItuZoneAlertEnable(text):
    config.set('DEFAULT','NEW_ITU_ZONE_ALERT_ENABLE',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setNewDXCCAlertEnable(text):
    config.set('DEFAULT','NEW_DXCC_ALERT_ENABLE',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setNewCallAlertEnable(text):
    config.set('DEFAULT','NEW_CALL_ALERT_ENABLE',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setNewCQZoneoOnBandAlertEnable(text):
    config.set('DEFAULT','NEW_CQ_ZONE_ON_BAND_ALERT_ENABLE',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setNewITUZoneOnBandAlertEnable(text):
    config.set('DEFAULT','NEW_ITU_ZONE_ON_BAND_ALERT_ENABLE',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setNewDXCCOnBandAlertEnable(text):
    config.set('DEFAULT','NEW_DXCC_ON_BAND_ALERT_ENABLE',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setNewCallOnBandAlertEnable(text):
    config.set('DEFAULT','NEW_CALL_ON_BAND_ALERT_ENABLE',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setTelnetServer(text):
    config.set('DEFAULT','TELNET_SERVER',str(text))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

def setTelnetPort(port):
    config.set('DEFAULT','TELNET_PORT',str(port))
    with open('config/config.ini', 'w') as configfile:
        config.write(configfile)

