from fake_headers import Headers
import xmltodict
import json

import csv 
import requests
import sys
import time
import datetime

#Header fälschen
url = "https://www.hamqth.com/dxcc.php?callsign="

def callsign(Call):
    headers = Headers(os="mac", headers=True).generate()
    try:
        data ={}
        httpx = requests.get(url + Call, headers=headers)
        antwort = httpx.text
        o = json.dumps(xmltodict.parse(antwort))
        obj = json.loads(o)
        data['CALL'] = obj['HamQTH']['dxcc']['callsign']
        data['COUNTRY'] = obj['HamQTH']['dxcc']['name']
        data['CONT'] = obj['HamQTH']['dxcc']['continent']
        if 'cqz' in obj['HamQTH']['dxcc']:
            data['CQZ'] = obj['HamQTH']['dxcc']['cqz']
        if 'ituz' in obj['HamQTH']['dxcc']:
            data['ITUZ'] = obj['HamQTH']['dxcc']['ituz']
        data['DXCC'] = obj['HamQTH']['dxcc']['adif']
        return data
    except requests.exceptions.ConnectionError:
        print("ConnectionError")
        return False
