import socket
import struct
from tkinter import messagebox
import logging
import sys

import helper.pywsjtx.pywsjtx
import helper.functions
import helper.config
import helper.hamradio
import CLASS.DATABASE
from helper.pywsjtx.pywsjtx.wsjtx_packets import InvalidPacket

conf = helper.config.initconfig()

Database = CLASS.DATABASE.DATABASE()
Database.connect(conf['DATABASE_URL'],'root',conf['DATABASE_PW'])

MULTICAST = conf['MULTICAST_ENABLE']

ip = conf['MULTICAST_IP']
port = int(conf['MULTICAST_PORT'])
server_address = (ip, port)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
if MULTICAST:
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    mreq = struct.pack('4sl', socket.inet_aton(ip), socket.INADDR_ANY)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
sock.bind(server_address)

def decode():
    try:
        radioinformations = {}
        radioinformations['BAND'] = ""
        while True:
            data = {}  
            fileContent, addr = sock.recvfrom(4096)
            packet = helper.pywsjtx.pywsjtx.WSJTXPacketClassFactory.from_udp_packet(addr,fileContent)
            if isinstance(packet, InvalidPacket):
                logging.warning("Invalid Packet: ")
            else:
                if packet.pkt_type == 1:
                    radioinformations['BAND'] = helper.functions.convert_freq_to_band(packet.dial_frequency/1000000)

                if packet.pkt_type == 2 and packet.message is not None:
                    data['BAND'] = radioinformations['BAND']
                    x = packet.message.split()
                    if len(x) < 2:      #Wrong packages filter
                        logging.info("Wrong packet Message: " + packet.message)
                    elif packet.message[0:2] == "CQ":
                        index = 0
                        if len(x[1]) <= 3:
                            index += 1
                        if helper.functions.IsACall(x[1+index].strip('<').strip('>')):
                            data['TYPE'] = "CQ"
                            data['CQ'] = x[1]
                            # @todo
                            # @body who is called?
                            data['CALL_SENT'] = x[1+index]
                            try:
                                if helper.functions.IsAGridsquare(x[2+index]):
                                    data['GRIDSQUARE'] = x[2+index]
                            except:
                                logging.warning("Cant detect a Grid")
                            Database.insertWsjtxQso(data)
                            colors = searchForColor(data['CALL_SENT'].strip('<').strip('>'),radioinformations['BAND'])
                            send(packet.wsjtx_id,data['CALL_SENT'],addr,colors)
                    elif packet.message[-3] == "+" or packet.message[-3] == "-":
                        if helper.functions.IsACall(x[0]):
                            if helper.functions.IsACall(x[1]):
                                data['TYPE'] = "RAPPORT"
                                data['CALL_RCVD'] = x[0]
                                data['CALL_SENT'] = x[1]
                                if x[2][0] == "R":
                                    data['RST'] = x[2][1:]
                                else:
                                    data['RST'] = x[2]
                                Database.insertWsjtxQso(data)
                                colors = searchForColor(data['CALL_RCVD'].strip('<').strip('>'),radioinformations['BAND'])
                                send(packet.wsjtx_id,data['CALL_RCVD'],addr,colors)
                                colors = searchForColor(data['CALL_SENT'].strip('<').strip('>'),radioinformations['BAND'])
                                send(packet.wsjtx_id,data['CALL_SENT'],addr,colors)
                    elif packet.message[len(packet.message)-2:] == "73":
                        if helper.functions.IsACall(x[0]):
                            if helper.functions.IsACall(x[1]):
                                data['TYPE'] = "FINISH"
                                data['CALL_RCVD'] = x[0]
                                data['CALL_SENT'] = x[1]
                                Database.insertWsjtxQso(data)
                                colors = searchForColor(data['CALL_RCVD'].strip('<').strip('>'),radioinformations['BAND'])
                                send(packet.wsjtx_id,data['CALL_RCVD'],addr,colors)
                                colors = searchForColor(data['CALL_SENT'].strip('<').strip('>'),radioinformations['BAND'])
                                send(packet.wsjtx_id,data['CALL_SENT'],addr,colors)
                    elif helper.functions.IsACall(x[0]) and helper.functions.IsACall(x[1]):
                        data['TYPE'] = "ANSWER"
                        data['CALL_RCVD'] = x[0]
                        data['CALL_SENT'] = x[1]
                        try:
                            if helper.functions.IsAGridsquare(x[2]):
                                data['GRIDSQUARE'] = x[2]
                        except:
                            logging.warning("Cant detect a Grid")
                        Database.insertWsjtxQso(data)
                        colors = searchForColor(data['CALL_RCVD'].strip('<').strip('>'),radioinformations['BAND'])
                        send(packet.wsjtx_id,data['CALL_RCVD'],addr,colors)
                        colors = searchForColor(data['CALL_SENT'].strip('<').strip('>'),radioinformations['BAND'])
                        send(packet.wsjtx_id,data['CALL_SENT'],addr,colors)
                    else:
                        logging.error("Not defined: " + packet.message)
    except AttributeError:
        errortext = "AttributeError: Decoding from WSJT-X failed\n" + str(sys.exc_info())
        print(errortext)
        logging.error(errortext)
        messagebox.showerror(message=errortext, title = "Error")
    except IndexError:
        errortext = "IndexError: Decoding from WSJT-X failed\n" + str(sys.exc_info() + "\n Packet Message: " + packet.message)
        print(errortext)
        logging.error(errortext)
        messagebox.showerror(message=errortext, title = "Error")



def searchForColor(Call,Band):
    CallDXCC = helper.hamradio.searchDXCC(Call)
    #Call on Band
    if CallDXCC is False:
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.Red()
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.White()
    elif Database.searchCallAdif(Call,Band,False):
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.Green()
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.Black()
    #New Call on Band
    elif Database.searchCallAdif(Call,Band,True):
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.RGBA(0xFF,0xFF,0xFF,0x60)
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.Black()
    #New DXCC on Band
    elif Database.searchDXCC(CallDXCC['entityCode'],Band,True):
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.RGBA(0xFF,0xFF,0x80,0x60)
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.Black()
    #New ITU Zone on Band
    elif Database.searchITU(CallDXCC['itu'],Band,True):
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.RGBA(0xFF,0xFF,0x40,0x60)
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.Black()
    #New CQ Zone on Band
    elif Database.searchCQZ(CallDXCC['cq'],Band,False):
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.RGBA(0xFF,0xFF,0x00,0x60)
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.Black()
    #New Call
    elif Database.searchCallAdif(Call,Band,False):
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.RGBA(0xFF,0xFF,0xFF,0x00)
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.Black()
    #New DXCC
    elif Database.searchDXCC(CallDXCC['entityCode'],Band,False):
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.RGBA(0xFF,0xFF,0x80,0x00)
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.Black()
    #New ITU
    elif Database.searchITU(CallDXCC['itu'],Band,False):
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.RGBA(0xFF,0xFF,0x40,0x00)
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.Black()
    #New CQ Zone
    elif Database.searchCQZ(CallDXCC['cq'],Band,False):
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.Red()
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.Black()
    #Nothing Found
    else:
        backgroundColor = helper.pywsjtx.pywsjtx.QCOLOR.Red()
        textColor = helper.pywsjtx.pywsjtx.QCOLOR.White()

    return backgroundColor, textColor

def send(wsjtx_id,callsign,addr_port,Color):

    color_pkt = helper.pywsjtx.pywsjtx.HighlightCallsignPacket.Builder(wsjtx_id, callsign,
                                                                    Color[0],
                                                                    Color[1],
                                                                    True)


    sock.sendto(color_pkt,addr_port)

def testsend():
    




    sock.sendto(color_pkt,addr_port)



#https://gist.github.com/dksmiffs/96ddbfd11ad7349ab4889b2e79dc2b22
