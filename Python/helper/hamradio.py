import json
import re

def searchDXCC(Call):
    with open('Python/helper/dxccjson/dxcc.json') as f:
        data = json.load(f)
    for keys in data['dxcc']:
        m = re.match(keys['prefixRegex'],str(Call))
        if m:
            if m.end() > 3:
                return keys
    return False
