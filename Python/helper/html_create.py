import helper.config
import time
import datetime
import json
import CLASS.DATABASE

def create():
    conf = helper.config.initconfig()

    Database = CLASS.DATABASE.DATABASE()
    Database.connect(conf['DATABASE_URL'],'root',conf['DATABASE_PW'])

    html_header = """
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf8">
<TITLE> DXCC statistics of """ + conf['CALLSIGN'] + """ </TITLE>
<META NAME="GENERATOR" CONTENT="ADIFCheck ver. 0.3">
<style type="text/css">
<!--
.popis {color: #FFFFFF}
.hlava {
    color: #03010c;
    font-family: Verdana, AriSBal, Helvetica, sans-serif;
    font-size: 12px;
    font-weight: bold;
}
-->
</style>
</HEAD>
<BODY>
<BR>
<H1 ALIGN=CENTER> DXCC statistics</H1>
<BR>
    """

    html_table_start = """
<table border="1" cellpading="2" cellspacing="0" style="font-family: Courier;">
<tr>
<th>Entity Code</th>  
<th>Country Code</th>
<th>Name</th>
<th>Number of QSO</th>
</tr>
    
    """
    html_dxcc = ""
    with open('Python/helper/dxccjson/dxcc.json') as f:
        data = json.load(f)
    for keys in data['dxcc']:
        numberOfQSO = Database.getcountDXCC(keys['entityCode'])
        if numberOfQSO > 0:
            dxcc = """
    <tr>
        <td>"""+ str(keys['entityCode']) + """</td>
        <td>"""+ keys['countryCode'] + """</td>
        <td>"""+ keys['name'] + """</td>
        <td>"""+ str(numberOfQSO) + """</td>
    </tr>
            """
            html_dxcc = html_dxcc + dxcc

    html_table_end = """
</table>    
    
    """

    html_footer = """
<footer>
<p> """ + datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S") + """ </p>
  <p id="copyright">ADIF Check</p>
</footer>
    """

    html_builded = html_header + html_table_start + html_dxcc + html_table_end + html_footer

    with open('html/dxcc.html', 'w') as html:
        html.write(html_builded)
