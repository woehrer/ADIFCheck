#!/usr/bin/python
#-*- coding:utf-8 -*-

import helper.config
import helper.adif
import helper.dxccjson.dxcc
import helper.wsjtx
import helper.functions
import helper.html_create

import windows.info
import windows.config
import windows.telnet_window
import windows.wsjtx_window
import windows.searchlist_window
import windows.dxcluster_window

import CLASS.DATABASE

from tkinter import *
from tkinter import messagebox, filedialog
import time
import webbrowser
import threading

conf = helper.config.initconfig()
helper.functions.initlogger()

Database = CLASS.DATABASE.DATABASE()
Database.connect(conf['DATABASE_URL'],'root',conf['DATABASE_PW'])
Database.setup()

Qso_Json_List = []

# Ein Fenster erstellen
fenster = Tk()
# Den Fenstertitle erstellen
fenster.title("ADIF Auswertung")

helper.functions.MostWantedList(conf['DATABASE_URL'],'root',conf['DATABASE_PW'])

utczeit = ''
localzeit = ''

def loop1min():
    while True:
        Database.checkCallsigns()
        time.sleep(60)

prozess = threading.Thread(target=loop1min,args=())
prozess.start()


def tick1min():
    
    fenster.after(60000, tick1min)
    
    

def tick():
    utczeit = time.strftime('%H:%M:%S',time.gmtime())
    localzeit = time.strftime('%H:%M:%S')
    utcuhr.config(text = utczeit)
    localuhr.config(text = localzeit) 
    utcuhr.after(200, tick) 


def openfile():
    Path = filedialog.askopenfilename()
    helper.config.setlastpath(Path)
    Path_label.config(text=Path)
    #Label anpassen

def callback(url):
    webbrowser.open_new(url)

# Menü

def config_action():
    configWindow = Toplevel(fenster)
    configWindow.title("Configuration")
    windows.config.window(configWindow)

# Menüleiste erstellen 
menuleiste = Menu(fenster)

# Menü Datei und Help erstellen
datei_menu = Menu(menuleiste, tearoff=0)
help_menu = Menu(menuleiste, tearoff=0)

datei_menu.add_command(label="Config", command=config_action)
datei_menu.add_separator() # Fügt eine Trennlinie hinzu
datei_menu.add_command(label="Exit", command=fenster.quit)

help_menu.add_command(label="Info", command=windows.info.messagebox)

menuleiste.add_cascade(label="File", menu=datei_menu)
menuleiste.add_cascade(label="Help", menu=help_menu)

fenster.config(menu=menuleiste)   



#Labels erstellen

utc_label = Label(fenster, 
                text = "UTC",
                font=('Arial',30),
                fg='black',
                width = 10,
                height = 1)

local_label = Label(fenster, 
                text = "Lokal",
                font=('Arial',30),
                fg='black',
                width = 10,
                height = 1)


Path_label = Label(fenster, text = conf['lastPath'])
qso_anzahl_label = Label(fenster, text = "0")


DatabaseFrame = Frame(fenster)
DatabaseTextLabel = Label(DatabaseFrame, text="Path to Database")
DatabaseURLLabel = Label(DatabaseFrame, text= conf['DATABASE_URL'], cursor="hand2",fg='blue')

# Buttons erstellen

openfile_Button = Button(fenster, text = "File wählen", command=openfile, height = 2)
def adif_action():
    count = helper.adif.read(conf['lastPath'],conf['DATABASE_URL'],'root',conf['DATABASE_PW'])
    qso_anzahl_label.config(text = "Anzahl QSOs: " + str(count))
adif_button = Button(fenster, text="ADIF einlesen", command=adif_action, height = 2)
def wsjtx_action():
    wsjtxWindow = Toplevel(fenster)
    wsjtxWindow.title("WSJTX")
    windows.wsjtx_window.window(wsjtxWindow)
wsjtx_button = Button(fenster, text="WSJT-X Communication", command=wsjtx_action , height = 2, width=20)
def telnet_action():
    telnetWindow = Toplevel(fenster)
    telnetWindow.title("Telnet")
    windows.telnet_window.window(telnetWindow)
telnet_button = Button(fenster, text="TELNET Communication", command=telnet_action , height = 2, width=20)    
def cluster_action():
    clusterWindow = Toplevel(fenster)
    clusterWindow.title("Cluster")
    windows.dxcluster_window.window(clusterWindow)
Cluster_button = Button(fenster, text="Cluster", command=cluster_action , height = 2, width=20)    
def searchList_action():
    searchListWindow = Toplevel(fenster)
    searchListWindow.title("Search List")
    windows.searchlist_window.window(searchListWindow)
searchList_button = Button(fenster, text="Search List", command=searchList_action , height = 2, width=20)
def statistik_action():
    helper.html_create.create()
    webbrowser.open_new("html/dxcc.html")
statistik_button = Button(fenster, text="Statistik", command=statistik_action , height = 2, width=20)    



# Uhrzeit wird über ein Label angezeigt und mit pack im fenster gezeigt

utcuhr = Label(master=fenster,
            font=('Arial',30),
            fg='black',
            width = 10,
            height = 1)

localuhr = Label(master=fenster,
            font=('Arial',30),
            fg='black',
            width = 10,
            height = 1)


statusbar = Label(fenster, text="", bd=1, relief=SUNKEN, anchor=W)



utc_label.pack()
utcuhr.pack()
local_label.pack()
localuhr.pack() 
openfile_Button.pack()
Path_label.pack()
adif_button.pack()
qso_anzahl_label.pack()
wsjtx_button.pack()
telnet_button.pack()
Cluster_button.pack()
searchList_button.pack()
statistik_button.pack()
DatabaseFrame.pack()
DatabaseTextLabel.pack(side=LEFT)
DatabaseURLLabel.pack(side=RIGHT)
DatabaseURLLabel.bind("<Button-1>", lambda e: callback(conf['DATABASE_URL']))
statusbar.pack(side=BOTTOM, fill=X)
 
tick()
tick1min()

# In der Ereignisschleife auf Eingabe des Benutzers warten.

def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        fenster.destroy()

fenster.protocol("WM_DELETE_WINDOW", on_closing)

fenster.mainloop()


# Setup mit pyinstaller main.spec
