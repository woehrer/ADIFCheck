
from tkinter.constants import DISABLED, END
import tkinter
import CLASS.DATABASE
import helper.config
import helper.wsjtx
import threading
import logging
from tkinter import messagebox
import sys


def table(data,EntryListe):
    conf = helper.config.initconfig()
    i = 0
    for lbls in data:
        try:
            EntryListe[i][0].delete(0, END)
            if 'CALL_RCVD' in lbls:
                EntryListe[i][0].insert(0, lbls['CALL_RCVD'])
            else:
                EntryListe[i][0].insert(0, "CQ")

            EntryListe[i][1].delete(0, END)
            EntryListe[i][1].insert(0, lbls['BAND'])

            background, text = helper.functions.searchForColor(conf['DATABASE_URL'], 'root', conf['DATABASE_PW'], lbls['CALL_SENT'], lbls['BAND'])
            EntryListe[i][2].delete(0, END)
            EntryListe[i][2].insert(0, lbls['CALL_SENT'])
            EntryListe[i][2].config(bg = background,fg=text)

            EntryListe[i][3].delete(0, END)
            EntryListe[i][3].insert(0, lbls['TIMESTAMP'])

            i += 1
        except KeyError:
            errortext = "KeyError: Build the table\n" + str(sys.exc_info())
            print(errortext)
            logging.error(errortext)
            messagebox.showerror(message=errortext, title = "Error")


def window(fenster):
    Database = CLASS.DATABASE.DATABASE()
    conf = helper.config.initconfig()
    Database.connect(conf['DATABASE_URL'], 'root', conf['DATABASE_PW'])
    Label1 = tkinter.Label(fenster, text="CALL RCVD")
    Label2 = tkinter.Label(fenster, text="BAND")
    Label3 = tkinter.Label(fenster, text="CALL SENT")
    Label4 = tkinter.Label(fenster, text="TIMESTAMP")
    lblListe1 = [tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=30)]
    lblListe2 = [tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=30)]
    lblListe3 = [tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=30)]
    lblListe4 = [tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=30)]
    lblListe5 = [tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=30)]
    lblListe6 = [tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=30)]
    lblListe7 = [tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=30)]
    lblListe8 = [tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=30)]
    lblListe9 = [tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=10),
                 tkinter.Entry(fenster, width=30)]
    lblListe10 = [tkinter.Entry(fenster, width=10),
                  tkinter.Entry(fenster, width=10),
                  tkinter.Entry(fenster, width=10),
                  tkinter.Entry(fenster, width=30)]
    
    EntryListe = [lblListe1,
                  lblListe2,
                  lblListe3,
                  lblListe4,
                  lblListe5,
                  lblListe6,
                  lblListe7,
                  lblListe8,
                  lblListe9,
                  lblListe10]    

    prozess = threading.Thread(target=helper.wsjtx.decode, args=())

    statusbar = tkinter.Label(fenster, text="", bd=1, relief=tkinter.SUNKEN, anchor=tkinter.W)

    def tick():
        wsjtxButton.after(5000,tick)
        last10wsjtx = Database.getlast10wsjtx()
        if prozess.is_alive():
            wsjtxButton.config(bg='green')
            statusbar.config(text="is running", bg="green")
        else:
            wsjtxButton.config(bg='red')
            statusbar.config(text="is stopped", bg="red")
        if last10wsjtx:
            table(last10wsjtx, EntryListe)


    def start_wsjtx():
        prozess.start()
        wsjtxButton.config(background="green", command=DISABLED)

    wsjtxButton = tkinter.Button(fenster,text="connect", command=start_wsjtx)

    tick()

    wsjtxButton.grid(row=0, column=0)
    Label1.grid(row=1, column=0)
    Label2.grid(row=1, column=1)
    Label3.grid(row=1, column=2)
    Label4.grid(row=1, column=3)

    j = 0
    for lbl in EntryListe:
        lbl[0].grid(row=j+2, column=0)
        lbl[1].grid(row=j+2, column=1)
        lbl[2].grid(row=j+2, column=2)
        lbl[3].grid(row=j+2, column=3)
        j += 1

    statusbar.grid(row=j+4, column=0, columnspan=2, sticky='we')
