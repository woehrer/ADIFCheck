import logging
from tkinter.constants import DISABLED, END
import CLASS.DATABASE
import CLASS.TELNET
import helper.config
import tkinter
import threading
import helper.functions



def decode():
    Database = CLASS.DATABASE.DATABASE()
    Telnet = CLASS.TELNET.TELNET()
    conf = helper.config.initconfig()
    Telnet.connect(conf['TELNET_SERVER'],conf['TELNET_PORT'],conf['CALLSIGN'])
    Database.connect(conf['DATABASE_URL'],'root',conf['DATABASE_PW'])
    while True:
        data = Telnet.decode()
        if data:
            Database.insertTelnet(data)

def table(data,EntryListe):
    conf = helper.config.initconfig()
    i = 0
    for lbls in data:
        try:
            band =helper.functions.convert_freq_to_band(float(float(lbls['FREQ'])/1000))
        except ValueError:
            band = ""
            logging.warning("CalueError in telnet_window.py Freq: " + lbls['FREQ'])

        EntryListe[i][0].delete(0, END)
        EntryListe[i][0].insert(0, lbls['CALL_RCVD'])

        EntryListe[i][1].delete(0, END)
        EntryListe[i][1].insert(0, lbls['FREQ'])

        EntryListe[i][2].delete(0, END)
        EntryListe[i][2].insert(0, band)

        background, text = helper.functions.searchForColor(conf['DATABASE_URL'],'root',conf['DATABASE_PW'],lbls['CALL_SENT'],band)
        EntryListe[i][3].delete(0, END)
        EntryListe[i][3].insert(0, lbls['CALL_SENT'])
        EntryListe[i][3].config(bg = background,fg=text)

        EntryListe[i][4].delete(0, END)
        EntryListe[i][4].insert(0, lbls['TIMESTAMP'])

        i += 1

def window(fenster):
    Database = CLASS.DATABASE.DATABASE()
    conf = helper.config.initconfig()
    Database.connect(conf['DATABASE_URL'],'root',conf['DATABASE_PW'])
    Label1 = tkinter.Label(fenster, text="CALL RCVD")
    Label2 = tkinter.Label(fenster, text="FREQ")
    Label3 = tkinter.Label(fenster, text="BAND")
    Label4 = tkinter.Label(fenster, text="CALL SENT")
    Label5 = tkinter.Label(fenster, text="TIMESTAMP")
    lblListe1 = [tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=30)]
    lblListe2 = [tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=30)]
    lblListe3 = [tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=30)]
    lblListe4 = [tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=30)]
    lblListe5 = [tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=30)]
    lblListe6 = [tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=30)]
    lblListe7 = [tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=30)]
    lblListe8 = [tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=30)]
    lblListe9 = [tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=30)]
    lblListe10 = [tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=10),tkinter.Entry(fenster,width=30)]
    
    EntryListe = [lblListe1,lblListe2,lblListe3,lblListe4,lblListe5,lblListe6,lblListe7,lblListe8,lblListe9,lblListe10]    
    
    prozess = threading.Thread(target=decode,args=())

    statusbar = tkinter.Label(fenster, text="", bd=1, relief=tkinter.SUNKEN, anchor=tkinter.W)

    def tick():
        connectButton.after(5000,tick)
        last10Telnet = Database.getlast10Telnet()
        if prozess.is_alive():
            connectButton.config(bg='green')
            statusbar.config(text="is running",bg="green")
        else:
            connectButton.config(bg='red')
            statusbar.config(text="is stopped",bg="red")
        if last10Telnet:
            table(last10Telnet,EntryListe)

    def start_decode():
        prozess.start()
        connectButton.config(background="green",command=DISABLED)

    connectButton = tkinter.Button(fenster,text="connect",command=start_decode)

    tick()

    connectButton.grid(row=0,column=0)
    Label1.grid(row=1, column=0)
    Label2.grid(row=1, column=1)
    Label3.grid(row=1, column=2)
    Label4.grid(row=1, column=3)
    Label5.grid(row=1, column=4)


    j = 0
    for lbl in EntryListe:
        lbl[0].grid(row=j+2,column=0)
        lbl[1].grid(row=j+2,column=1)
        lbl[2].grid(row=j+2,column=2)
        lbl[3].grid(row=j+2,column=3)
        lbl[4].grid(row=j+2,column=4)
        j += 1

    statusbar.grid(row=j+4, column=0, columnspan=2, sticky='we')
