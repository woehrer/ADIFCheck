import tkinter
from tkinter.constants import LEFT, RIGHT
from tkinter import ttk
import helper.config

conf = helper.config.initconfig()


def window(fenster):
    def save():
        helper.config.setCallsign(CallsignEntry.get())
        helper.config.setlastpath(PathEntry.get())
        helper.config.setMulticastIP(MulticastIPEntry.get())
        helper.config.setMulticastPort(MulticastPortEntry.get())
        helper.config.setMulticastEnable(MulticastEnablechkValue.get())
        helper.config.setdatabaseURL(DatabaseURLEntry.get())
        helper.config.setNewItuZoneAlertEnable(NewITUZoneAlertEnablechkValue.get())
        helper.config.setNewCQZoneAlertEnable(NewCqZoneAlertEnablechkValue.get())
        helper.config.setNewDXCCAlertEnable(NewDXCCAlertEnablechkValue.get())
        helper.config.setNewCallAlertEnable(NewCallAlertEnablechkValue.get())
        helper.config.setNewITUZoneOnBandAlertEnable(NewITUZoneOnBandAlertEnablechkValue.get())
        helper.config.setNewCQZoneoOnBandAlertEnable(NewCQZoneOnBandAlertEnablechkValue.get())
        helper.config.setNewDXCCOnBandAlertEnable(NewDXCCOnBandAlertEnablechkValue.get())
        helper.config.setNewCallOnBandAlertEnable(NewCallOnBandAlertEnablechkValue.get())
        fenster.destroy()

    tabControl = ttk.Notebook(fenster)
    tab1 = ttk.Frame(tabControl)
    tab2 = ttk.Frame(tabControl)
    tab3 = ttk.Frame(tabControl)
    tab4 = ttk.Frame(tabControl)
    tabControl.add(tab1, text ='General')
    tabControl.add(tab2, text ='WSJT-X')
    tabControl.add(tab3, text ='Alerts')
    tabControl.add(tab4, text ='Telnet')
    tabControl.pack(expand = 1, fill ="both")

    #Callsign
    CallsignFrame = tkinter.Frame(tab1)
    CallsignLabel = tkinter.Label(CallsignFrame, text="Callsign")
    CallsignEntry = tkinter.Entry(CallsignFrame)
    CallsignEntry.insert(0, conf['CALLSIGN'])

    #ADIF Path
    PathFrame = tkinter.Frame(tab1)
    PathLabel = tkinter.Label(PathFrame, text="ADIF Path")
    PathEntry = tkinter.Entry(PathFrame)
    PathEntry.insert(0, conf['lastPath'])

    #MultiCast IP
    MulticastIPFrame = tkinter.Frame(tab2)
    MulticastIPLabel = tkinter.Label(MulticastIPFrame, text="Multicast IP")
    MulticastIPEntry = tkinter.Entry(MulticastIPFrame)
    MulticastIPEntry.insert(0, conf['MULTICAST_IP'])

    #MultiCast Port
    MulticastPortFrame = tkinter.Frame(tab2)
    MulticastPortLabel = tkinter.Label(MulticastPortFrame, text="Multicast Port")
    MulticastPortEntry = tkinter.Entry(MulticastPortFrame)
    MulticastPortEntry.insert(0, conf['MULTICAST_PORT'])

    #MultiCast Enable
    MulticastEnablechkValue = tkinter.BooleanVar()
    MulticastEnablechkValue.set(conf['MULTICAST_ENABLE'])
    MulticastEnableFrame = tkinter.Frame(tab2)
    MulticastEnableLabel = tkinter.Label(MulticastEnableFrame, text="Multicast Enable")
    MulticastEnableCheckbutton = tkinter.Checkbutton(MulticastEnableFrame,var=MulticastEnablechkValue)

    #Database Password
    DatabasePWFrame = tkinter.Frame(tab1)
    DatabasePWLabel = tkinter.Label(DatabasePWFrame, text="Database Password")
    DatabasePWEntry = tkinter.Entry(DatabasePWFrame)
    DatabasePWEntry.insert(0, conf['DATABASE_PW'])

    #Database URL
    DatabaseURLFrame = tkinter.Frame(tab1)
    DatabaseURLLabel = tkinter.Label(DatabaseURLFrame, text="Database URL")
    DatabaseURLEntry = tkinter.Entry(DatabaseURLFrame)
    DatabaseURLEntry.insert(0, conf['DATABASE_URL'])

    #New CQ Zone Alert Enable
    NewCqZoneAlertEnablechkValue = tkinter.BooleanVar()
    NewCqZoneAlertEnablechkValue.set(conf['NEW_CQ_ZONE_ALERT_ENABLE'])
    NewCqZoneAlertEnableFrame = tkinter.Frame(tab3)
    NewCqZoneAlertEnableLabel = tkinter.Label(NewCqZoneAlertEnableFrame, text="New CQ Zone Alert Enable")
    NewCqZoneAlertEnalbeCheckbutton = tkinter.Checkbutton(NewCqZoneAlertEnableFrame,var=NewCqZoneAlertEnablechkValue)

    #New ITU Zone Alert Enable
    NewITUZoneAlertEnablechkValue = tkinter.BooleanVar()
    NewITUZoneAlertEnablechkValue.set(conf['NEW_ITU_ZONE_ALERT_ENABLE'])
    NewITUZoneAlertEnableFrame = tkinter.Frame(tab3)
    NewITUZoneAlertEnableLabel = tkinter.Label(NewITUZoneAlertEnableFrame, text="New ITU Zone Alert Enable")
    NewITUZoneAlertEnableCheckbutton = tkinter.Checkbutton(NewITUZoneAlertEnableFrame,var=NewITUZoneAlertEnablechkValue)

    #New DXCC Alert Enable
    NewDXCCAlertEnablechkValue = tkinter.BooleanVar()
    NewDXCCAlertEnablechkValue.set(conf['NEW_DXCC_ALERT_ENABLE'])
    NewDXCCAlertEnableFrame = tkinter.Frame(tab3)
    NewDXCCAlertEnableLabel = tkinter.Label(NewDXCCAlertEnableFrame, text="New DXCC Alert Enable")
    NewDXCCAlertEnableCheckbutton = tkinter.Checkbutton(NewDXCCAlertEnableFrame,var=NewDXCCAlertEnablechkValue)

    #New Call Alert Enable
    NewCallAlertEnablechkValue = tkinter.BooleanVar()
    NewCallAlertEnablechkValue.set(conf['NEW_CALL_ALERT_ENABLE'])
    NewCallAlertEnableFrame = tkinter.Frame(tab3)
    NewCallAlertEnableLabel = tkinter.Label(NewCallAlertEnableFrame, text="New Call Alert Enable")
    NewCallAlertEnableCheckbutton = tkinter.Checkbutton(NewCallAlertEnableFrame,var=NewCallAlertEnablechkValue)

    #New CQ Zone on Band Alert Enable
    NewCQZoneOnBandAlertEnablechkValue = tkinter.BooleanVar()
    NewCQZoneOnBandAlertEnablechkValue.set(conf['NEW_CQ_ZONE_ON_BAND_ALERT_ENABLE'])
    NewCQZoneOnBandAlertEnableFrame = tkinter.Frame(tab3)
    NewCQZoneOnBandAlertEnableLabel = tkinter.Label(NewCQZoneOnBandAlertEnableFrame, text="New CQ Zone on Band Alert Enable")
    NewCQZoneOnBandAlertEnableCheckbutton = tkinter.Checkbutton(NewCQZoneOnBandAlertEnableFrame,var=NewCQZoneOnBandAlertEnablechkValue)

    #New ITU Zone on Band Alert Enable
    NewITUZoneOnBandAlertEnablechkValue = tkinter.BooleanVar()
    NewITUZoneOnBandAlertEnablechkValue.set(conf['NEW_ITU_ZONE_ON_BAND_ALERT_ENABLE'])
    NewITUZoneOnBandAlertEnableFrame = tkinter.Frame(tab3)
    NewITUZoneOnBandAlertEnableLabel = tkinter.Label(NewITUZoneOnBandAlertEnableFrame, text="New ITU Zone on Band Alert Enable")
    NewITUZoneOnBandAlertEnableCheckbutton = tkinter.Checkbutton(NewITUZoneOnBandAlertEnableFrame,var=NewITUZoneOnBandAlertEnablechkValue)

    #New DXCC on Band Alert Enable
    NewDXCCOnBandAlertEnablechkValue = tkinter.BooleanVar()
    NewDXCCOnBandAlertEnablechkValue.set(conf['NEW_DXCC_ON_BAND_ALERT_ENABLE'])
    NewDXCCOnBandAlertEnableFrame = tkinter.Frame(tab3)
    NewDXCCOnBandAlertEnableLabel = tkinter.Label(NewDXCCOnBandAlertEnableFrame, text="New DXCC on Band Alert Enable")
    NewDXCCOnBandAlertEnableCheckbutton = tkinter.Checkbutton(NewDXCCOnBandAlertEnableFrame,var=NewDXCCOnBandAlertEnablechkValue)

    #New Call on Band Alert Enable
    NewCallOnBandAlertEnablechkValue = tkinter.BooleanVar()
    NewCallOnBandAlertEnablechkValue.set(conf['NEW_CALL_ON_BAND_ALERT_ENABLE'])
    NewCallOnBandAlertEnableFrame = tkinter.Frame(tab3)
    NewCallOnBandAlertEnableLabel = tkinter.Label(NewCallOnBandAlertEnableFrame, text="New Call on Band Alert Enable")
    NewCallOnBandAlertEnableCheckbutton = tkinter.Checkbutton(NewCallOnBandAlertEnableFrame,var=NewCallOnBandAlertEnablechkValue)

    #Telnet Server
    TelnetServerFrame = tkinter.Frame(tab4)
    TelnetServerLabel = tkinter.Label(TelnetServerFrame, text="Telnet Server")
    TelnetServerEntry = tkinter.Entry(TelnetServerFrame)
    TelnetServerEntry.insert(0, conf['TELNET_SERVER'])

    #Telnet Port
    TelnetPortFrame = tkinter.Frame(tab4)
    TelnetPortLabel = tkinter.Label(TelnetPortFrame, text="Telnet Port")
    TelnetPortEntry = tkinter.Entry(TelnetPortFrame)
    TelnetPortEntry.insert(0, conf['TELNET_PORT'])


    saveButton = tkinter.Button(fenster,text="save",command=save)



    #Tab 1
    #Callsign
    CallsignFrame.pack()
    CallsignLabel.pack(side=LEFT)
    CallsignEntry.pack(side=RIGHT)
    #ADIF Path
    PathFrame.pack()
    PathLabel.pack(side=LEFT)
    PathEntry.pack(side=RIGHT)
    #Database PW
    DatabasePWFrame.pack()
    DatabasePWLabel.pack(side=LEFT)
    DatabasePWEntry.pack(side=RIGHT)
    #Database URL
    DatabaseURLFrame.pack()
    DatabaseURLLabel.pack(side=LEFT)
    DatabaseURLEntry.pack(side=RIGHT)

    #Tab 2
    #MultiCast IP
    MulticastIPFrame.pack()
    MulticastIPLabel.pack(side=LEFT)
    MulticastIPEntry.pack(side=RIGHT)
    #MultiCast Port
    MulticastPortFrame.pack()
    MulticastPortLabel.pack(side=LEFT)
    MulticastPortEntry.pack(side=RIGHT)
    #MultiCast Enable
    MulticastEnableFrame.pack()
    MulticastEnableLabel.pack(side=LEFT)
    MulticastEnableCheckbutton.pack(side=RIGHT)
    #Database PW
    DatabasePWFrame.pack()
    DatabasePWLabel.pack(side=LEFT)
    DatabasePWEntry.pack(side=RIGHT)
    #Database URL
    DatabaseURLFrame.pack()
    DatabaseURLLabel.pack(side=LEFT)
    DatabaseURLEntry.pack(side=RIGHT)
    
    #Tab 3
    #New CQ Zone Alert Enable
    NewCqZoneAlertEnableFrame.pack()
    NewCqZoneAlertEnableLabel.pack(side=LEFT)
    NewCqZoneAlertEnalbeCheckbutton.pack(side=RIGHT)
    #New ITU Zone Alert Enable
    NewITUZoneAlertEnableFrame.pack()
    NewITUZoneAlertEnableLabel.pack(side=LEFT)
    NewITUZoneAlertEnableCheckbutton.pack(side=RIGHT)
    #New DXCC Alert Enable
    NewDXCCAlertEnableFrame.pack()
    NewDXCCAlertEnableLabel.pack(side=LEFT)
    NewDXCCAlertEnableCheckbutton.pack(side=RIGHT)
    #New Call Alert Enable
    NewCallAlertEnableFrame.pack()
    NewCallAlertEnableLabel.pack(side=LEFT)
    NewCallAlertEnableCheckbutton.pack(side=RIGHT)
    #New CQ Zone on Band Alert Enable
    NewCQZoneOnBandAlertEnableFrame.pack()
    NewCQZoneOnBandAlertEnableLabel.pack(side=LEFT)
    NewCQZoneOnBandAlertEnableCheckbutton.pack(side=RIGHT)
    #New ITU Zone on Band Alert Enable
    NewITUZoneOnBandAlertEnableFrame.pack()
    NewITUZoneOnBandAlertEnableLabel.pack(side=LEFT)
    NewITUZoneOnBandAlertEnableCheckbutton.pack(side=RIGHT)    
    #New DXCC Zone on Band Alert Enable
    NewDXCCOnBandAlertEnableFrame.pack()
    NewDXCCOnBandAlertEnableLabel.pack(side=LEFT)
    NewDXCCOnBandAlertEnableCheckbutton.pack(side=RIGHT)  
    #New DXCC Zone on Band Alert Enable
    NewCallOnBandAlertEnableFrame.pack()
    NewCallOnBandAlertEnableLabel.pack(side=LEFT)
    NewCallOnBandAlertEnableCheckbutton.pack(side=RIGHT)  

    #Tab 4
    #Telnet Server
    TelnetServerFrame.pack()
    TelnetServerLabel.pack(side=LEFT)
    TelnetServerEntry.pack(side=RIGHT)
    #Telnet Port
    TelnetPortFrame.pack()
    TelnetPortLabel.pack(side=LEFT)
    TelnetPortEntry.pack(side=RIGHT)

    saveButton.pack(padx=10)
