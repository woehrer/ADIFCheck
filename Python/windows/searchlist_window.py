from tkinter.constants import DISABLED, END
import tkinter
import CLASS.DATABASE
import logging
from tkinter import messagebox
import sys

import helper.config

def window(fenster):
    Database = CLASS.DATABASE.DATABASE()
    conf = helper.config.initconfig()
    Database.connect(conf['DATABASE_URL'],'root',conf['DATABASE_PW'])
    Label1 = tkinter.Label(fenster, text="CALL RCVD")

    insertdata = {}

    statusbar = tkinter.Label(fenster, text="", bd=1, relief=tkinter.SUNKEN, anchor=tkinter.W)

    lblListe1 = [tkinter.Entry(fenster,width=10)]
    lblListe2 = [tkinter.Entry(fenster,width=10)]
    lblListe3 = [tkinter.Entry(fenster,width=10)]
    lblListe4 = [tkinter.Entry(fenster,width=10)]
    lblListe5 = [tkinter.Entry(fenster,width=10)]
    lblListe6 = [tkinter.Entry(fenster,width=10)]
    lblListe7 = [tkinter.Entry(fenster,width=10)]
    lblListe8 = [tkinter.Entry(fenster,width=10)]
    lblListe9 = [tkinter.Entry(fenster,width=10)]
    lblListe10 = [tkinter.Entry(fenster,width=10)]
    
    EntryListe = [lblListe1,lblListe2,lblListe3,lblListe4,lblListe5,lblListe6,lblListe7,lblListe8,lblListe9,lblListe10]    

    statusbar = tkinter.Label(fenster, text="", bd=1, relief=tkinter.SUNKEN, anchor=tkinter.W)

    def save():
        i = 0
        for lbls in EntryListe:
            insertdata[i] = lbls[0].get()
            i += 1
        Database.insertSearchlist(insertdata)

    saveButton = tkinter.Button(fenster,text="save",command=save)


    saveButton.grid(row=0,column=0)
    Label1.grid(row=1, column=0)

    data = Database.getSearchList()
    if not data:
        save()
        data = Database.getSearchList()
    #print(data)

    j = 0
    for lbl in EntryListe:
        jsondata = data[0]
        lbl[0].insert(0, jsondata[str(j)])
        lbl[0].grid(row=j+2,column=0)
        j += 1

    statusbar.grid(row=j+4, column=0, columnspan=2, sticky='we')
