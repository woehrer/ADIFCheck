
# Change Log
## [0.2.1] - yyyy-mm-dd

### Added

### Changed

### Fixed

## [0.2.0] - 2021-12-23

### Added
   - Color for Calls in Telnet and WSJTX Window [Issue 108](https://github.com/woehrer12/ADIFCheck/issues/108)
   - Database for all Recived Callsigns [Issue 18](https://github.com/woehrer12/ADIFCheck/issues/18)
   - Insert DXCluster with window [Issue 109](https://github.com/woehrer12/ADIFCheck/issues/109)
   - Install Skript added for ArangoDB in Docker or local [Issue 130](https://gitlab.com/woehrer/ADIFCheck/-/issues/130)
### Changed

### Fixed
   - Fixed Telnet Timestamp [Issue 70](https://github.com/woehrer12/ADIFCheck/issues/70)
   - insert missing CALL_RCVD Color in 73 Message [Issue 104](https://github.com/woehrer12/ADIFCheck/issues/104)
   - Fixed Redefining built-in config.py [Issue 116](https://github.com/woehrer12/ADIFCheck/issues/116)
   - Fixes global states [Issue 115](https://github.com/woehrer12/ADIFCheck/issues/115)
   - Fix Telnet Exeption rised [Issue 100](https://github.com/woehrer12/ADIFCheck/issues/100)
   - Fixed Typing Error Search List [Issue 122](https://gitlab.com/woehrer/ADIFCheck/-/issues/122)
   - Fix the error when open the SeachList Window [Issue 128](https://gitlab.com/woehrer/ADIFCheck/-/issues/128)


## [0.1.3] - 2021-07-26
 
### Added
   - Insert a SearchList Window [Issue 10](https://github.com/woehrer12/ADIFCheck/issues/10)
   - Own Class for DATABASE [Issue 79](https://github.com/woehrer12/ADIFCheck/issues/79)
   - Telnet Stream Auslesen [Issue 3](https://github.com/woehrer12/ADIFCheck/issues/3)
   - Own Window for WSJTX [Issue 86](https://github.com/woehrer12/ADIFCheck/issues/86)
   - Statistik HTML [Issue 6](https://github.com/woehrer12/ADIFCheck/issues/6)
   - Insert search LAT LON from Gridsquare [Issue 15](https://github.com/woehrer12/ADIFCheck/issues/15)
### Changed
   - Remove frequency to band from hamradio.py [Issue 82](https://github.com/woehrer12/ADIFCheck/issues/82)
   - Change all Logger to Logging the same function on all positions [Issue 19](https://github.com/woehrer12/ADIFCheck/issues/19)
### Fixed
   - Rufzeichen -> Callsign; Pfad -> Path
   - Wrong package message from wsjtx bring to rise exception enter a if
   - Empty the Database when start [Issue 85](https://github.com/woehrer12/ADIFCheck/issues/85)

## [0.1.2] - 2021-07-21
 
### Added    
   - Added a Error Messagbox for Config and Database [Issue 51](https://github.com/woehrer12/ADIFCheck/issues/51)
   - Config for the Alerter without Issue
   
### Changed

### Fixed
   - Fix the Install Problem with first install curl

## [0.1.1] - 2021-07-20
 
### Added    
   - Select the Color in Own function [Issue 54](https://github.com/woehrer12/ADIFCheck/issues/54)
   - different Colors for alerts [Issue 54](https://github.com/woehrer12/ADIFCheck/issues/54)
   - Download MostWanted List from Clublog and save in Database [Issue 59](https://github.com/woehrer12/ADIFCheck/issues/59)
   - Added Link to Database Web on main [Issue 41](https://github.com/woehrer12/ADIFCheck/issues/41)
   
### Changed
   - Convert the Database to one Database [Issue 69](https://github.com/woehrer12/ADIFCheck/issues/69)
### Fixed
   - Fix Comparsion Syntax Style [Issue 35](https://github.com/woehrer12/ADIFCheck/issues/35)
   - Fix Try Except Pass in adif.py [Issue 49](https://github.com/woehrer12/ADIFCheck/issues/49)
   - Call Rcvd request DXCC and make a timestamp [Issue 60](https://github.com/woehrer12/ADIFCheck/issues/60)

## [0.1.0] - 2021-07-19
 
### Added
   - Check is a Gridsquare [Issue 29](https://github.com/woehrer12/ADIFCheck/issues/29)
   - insert dcxx-json and Search DXCC from wsjtx input Calls and Save in the Database [Issue 1](https://github.com/woehrer12/ADIFCheck/issues/1)
   - install script for the ArandoDB and add a config for the password [Issue 22](https://github.com/woehrer12/ADIFCheck/issues/22)    

### Changed
   - Start Backend Decoding with Multithread [Issue 20](https://github.com/woehrer12/ADIFCheck/issues/20)
 
### Fixed
   - Emptry Folders added for logs and config [Issue 43](https://github.com/woehrer12/ADIFCheck/issues/43)
   - Fixed Duplicate key MY_COUNTRY in dictionary [Issue 34](https://github.com/woehrer12/ADIFCheck/issues/34)
   - Calls with '<' or '>' from the wsjtx [Issue 16](https://github.com/woehrer12/ADIFCheck/issues/16)
   - ArangoDB URL als Setting [Issue 16](https://github.com/woehrer12/ADIFCheck/issues/16)


## [0.0.1] - 2021-07-18
 
### Added
   - enhaucement with Issues link
### Changed
   - changes with Issues link
### Fixed
   - bug with Issues link

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
