# ADIFCheck

[![CodeFactor](https://www.codefactor.io/repository/github/woehrer12/adifcheck/badge)](https://www.codefactor.io/repository/github/woehrer12/adifcheck)

## Functions
 - Read a adif file and put it all QSOs into a Database.
 - Connect to WSJTX via UDP. Scan the Call that resicving and highligting it red when not in the database


## Install

```bash
git clone https://gitlab.com/woehrer/ADIFCheck.git
cd ADIFCheck/
./install.sh
```
set a Database password. Importent to set the password in the config too. The default user is: "root" and password is: "openSesame"

The Database URL is: [http://127.0.0.1:8529](http://127.0.0.1:8529)

## Start

### On Ubuntu:
```bash
./start.sh
```
# Dokumentation
## Main window
![main](/images/main.png)

## Settings window
![config1](/images/config1.png) ![config2](/images/config2.png) ![config3](/images/config3.png) ![config2](/images/config4.png)

## WSJT-X window
![wsjtx](images/wsjtx.png)

## Telnet window
![telnet](images/telnet.png)

## Settings WSJT-X
![settings_wsjtx](/images/settings_wsjtx.png)


## WSJT-X

### Colors:
<p style="background-color:#FF0000;"><span style="color:black">New CQ Zone</span></p>
<p style="background-color:#FF4000;"><span style="color:black">New ITU Zone</span></p>
<p style="background-color:#FF8000;"><span style="color:black">New DXCC</span></p>
<p style="background-color:#FFFF00;"><span style="color:black">New Call</span></p>
<p style="background-color:#FF0060;"><span style="color:black">New CQ Zone on Band</span></p>
<p style="background-color:#FF4060;"><span style="color:black">New ITU Zone on Band</span></p>
<p style="background-color:#FF8060;"><span style="color:black">New DXCC on Band</span></p>
<p style="background-color:#FFFF60;"><span style="color:black">New Call on Band</span></p>
<p style="background-color:green;"><span style="color:black">Call in Log</span></p>
