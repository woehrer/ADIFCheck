#https://www.arangodb.com/download-major/ubuntu/
#Docker sudo docker run -p 8529:8529 -e ARANGO_ROOT_PASSWORD=openSesame arangodb/arangodb:3.7.12
echo "Installing ArangoDB"
echo "0 = Install ArangoDB locally"
echo "1 = Install ArangoDB on Docker"
echo "2 = Install ArangoDB on Docker for Raspberry (only for development)"
echo "enter your choice"
read -r choice
git pull
git clone https://github.com/woehrer12/skripte.git ~/skripte
if [ $choice -eq 0 ]; then
    echo "Installing ArangoDB locally"
    sudo apt-get update
    sudo apt-get install -y arangodb3
    sudo systemctl start arangodb
    sudo systemctl enable arangodb
    sudo systemctl status arangodb
    echo "ArangoDB installed"
elif [ $choice -eq 1 ]; then
    echo "Installing ArangoDB on Docker"
    ~/skripte/docker-install.sh
    sudo docker run -e ARANGO_ROOT_PASSWORD=openSesame -p 8529:8529 -d arangodb/arangodb
    echo "ArangoDB installed on Docker"
elif [ $choice -eq 2 ]; then
    echo "Installing ArangoDB on Raspberry"
    ~/skripte/docker-install.sh
    sudo docker run -e ARANGO_ROOT_PASSWORD=openSesame -p 8529:8529 -d programmador/arangodb:3.8.0-devel
    echo "ArangoDB installed on Raspberry"
fi
git submodule init
git submodule update
sudo apt install python3 python3-pip python3-tk -y
pip3 install --upgrade tk hamutils pyArango pyhamtools fake_headers xmltodict